package info.sandlovers.gerbil.coat.genetics;

import java.util.regex.Pattern;

public class Color {
    public final String name;
    public final String geneRegexp;
    public final String sample;
    public final CharSequence htmlSample;
    public final Pattern pattern;

    public Color(String name, String geneRegexp) {
        this.name = name;
        this.geneRegexp = geneRegexp;
        this.pattern = Pattern.compile('^' + geneRegexp + '$');

        StringBuilder builder = new StringBuilder(geneRegexp);
        for (int i = 0; i < Genes.CODE_LENGTH; ++i) {
            if (builder.charAt(i) == '.') {
                builder.setCharAt(i, '-');
            }
            if (builder.charAt(i) == '[') {
                builder.delete(i + 2, builder.indexOf("]") + 1);
                builder.deleteCharAt(i);
            }
        }
        this.sample = builder.toString();
        this.htmlSample = Genes.getString(this.sample);
    }
}
