package info.sandlovers.gerbil.coat.genetics;

import java.util.Arrays;

public class Genotype {
    public final char[] genotype;

    public Genotype(char[] genotype) {
        this.genotype = genotype;
    }

    public static Genotype canonized(char[] genotype) {
        char[] canonized = new char[Genes.CODE_LENGTH];
        for (int i = 0; i < Genes.CODE_LENGTH; i += 2) {
            boolean shouldSwitch = false;
            if (Character.isLowerCase(genotype[i]) && Character.isUpperCase(genotype[i+1])) {
                shouldSwitch = true;
            }
            if (Character.isLowerCase(genotype[i]) && Character.isLowerCase(genotype[i + 1])
                    && genotype[i] > genotype[i+1]) {
                shouldSwitch = true;
            }

            canonized[i] = genotype[shouldSwitch ? i+1 : i];
            canonized[i+1] = genotype[shouldSwitch ? i : i+1];
        }
        return new Genotype(canonized);
    }

    public static Genotype dashLess(char[] genotype) {
        char[] dashLess = new char[Genes.CODE_LENGTH];
        for (int i = 0; i < Genes.CODE_LENGTH; ++i) {
            dashLess[i] = genotype[i] == '-' ? Genes.getDominantAllele(i / 2) : genotype[i];
        }
        return canonized(dashLess);
    }

    @Override
    public String toString() {
        return String.valueOf(genotype);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(genotype);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Genotype) {
            return Arrays.equals(genotype, ((Genotype) o).genotype);
        } else {
            return false;
        }
    }
}
