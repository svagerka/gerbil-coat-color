package info.sandlovers.gerbil.coat.genetics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Children extends ArrayList<Children.ColorInfo> {
    public static final String KEY_COLOR_NAME = "NAME";
    public static final String KEY_COLOR_PERCENTAGE = "PERCENTAGE";
    public static final String KEY_GENES = "GENES";

    static final Comparator<ColorInfo> CHILD_INFO_COMPARATOR = new Comparator<ColorInfo>() {
        @Override
        public int compare(ColorInfo color1, ColorInfo color2) {
            return color2.count - color1.count;
        }
    };


    public Children(Genotype mom, Genotype dad) {
        List<Genotype> children = new ArrayList<Genotype>();
        generateChildren(children, new char[Genes.CODE_LENGTH], 0,
                Genotype.dashLess(mom.genotype).genotype,
                Genotype.dashLess(dad.genotype).genotype);

        Map<Color, ColorInfo> colors = groupColors(children);
        finalizeColors(children, colors);

        Collections.sort(this, CHILD_INFO_COMPARATOR);
    }

    private void finalizeColors(List<Genotype> children, Map<Color, ColorInfo> colors) {
        for (ColorInfo colorInfo : colors.values()) {
            colorInfo.finalize(children.size());
            add(colorInfo);
        }
    }

    private Map<Color, ColorInfo> groupColors(List<Genotype> children) {
        Map<Color, ColorInfo> colors = new HashMap<Color, ColorInfo>();
        for (Genotype child : children) {
            Color color = ColorDatabase.getColorByGenotype(child);
            ColorInfo colorInfo = colors.get(color);
            if (colorInfo == null) {
                colorInfo = new ColorInfo(color);
                colors.put(color, colorInfo);
            }
            colorInfo.add(child);
        }
        return colors;
    }


    private void generateChildren(List<Genotype> resultArray, char[] partialCode, int n,
                                         char[] mom, char[] dad) {

        if (Genes.CODE_LENGTH == n) {
            resultArray.add(Genotype.canonized(partialCode));
            return;
        }

        for (int i = 0; i < 2; ++i) {
            if (i == 1 && mom[n] == mom[n + 1]) {
                break;
            }
            partialCode[n] = mom[n + i];
            for (int j = 0; j < 2; ++j) {
                if (j == 1 && dad[n] == dad[n + 1]) {
                    break;
                }
                partialCode[n + 1] = dad[n + j];
                generateChildren(resultArray, partialCode, n + 2, mom, dad);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (ColorInfo colorInfo : this) {
            builder.append(colorInfo.get(KEY_COLOR_PERCENTAGE))
                    .append(colorInfo.get(KEY_COLOR_NAME)).append('\n');
        }
        return builder.toString();
    }

    class ColorInfo extends HashMap<String, CharSequence> {
        int count;
        private final Color color;

        ColorInfo(Color color) {
            this.count = 0;
            this.color = color;
        }

        void add(Genotype genotype) {
            ++count;
        }

        void finalize(int total) {
            put(KEY_COLOR_NAME, color != null ? color.name : "");
            put(KEY_GENES, color != null ? color.htmlSample : "");
            put(KEY_COLOR_PERCENTAGE, String.format("%.02f %% - ", 100.0 * count / total));
        }
    }
}
