package info.sandlovers.gerbil.coat.genetics;

import android.text.Html;
import android.text.Spanned;

public class Genes {
    public static final int GENE_COUNT = 6;
    public static final int CODE_LENGTH = GENE_COUNT * 2;

    private static final String[] ALLELES = {
            "Aa-", "Cbh-", "Dd-", "Eef-", "Gg-", "Pp-"
    };

    private static final String COLOUR_POINT_BURMESE_HTML = "c<sup><small>b</small></sup>";
    private static final String COLOUR_POINT_HIMALAYAN_HTML = "c<sup><small>h</small></sup>";
    private static final String SCHIMMEL_HTML = "e<sup><small>f</small></sup>";

    private static final Spanned COLOUR_POINT_BURMESE_SPANNED =
            Html.fromHtml(COLOUR_POINT_BURMESE_HTML);
    private static final Spanned COLOUR_POINT_HIMALAYAN_SPANNED =
            Html.fromHtml(COLOUR_POINT_HIMALAYAN_HTML);
    private static final Spanned SCHIMMEL_SPANNED =
            Html.fromHtml(SCHIMMEL_HTML);

    public static char getDominantAllele(int gene) {
        return ALLELES[gene].charAt(0);
    }

    public static char getNextAllele(int gene, char allele) {
        return ALLELES[gene].charAt((ALLELES[gene].indexOf(allele) + 1) % ALLELES[gene].length());
    }

    public static char getAllele(CharSequence label) {
        String labelString = label.toString();
        if (label.length() == 1) {
            return label.charAt(0);
        } else if ("cb".equals(labelString)) {
            return 'b';
        } else if ("ch".equals(labelString)) {
            return 'h';
        } else if ("ef".equals(labelString)) {
            return 'f';
        } else {
            throw new IllegalStateException();
        }
    }

    public static CharSequence getAlleleLabel(char allele) {
        switch (allele) {
            case 'b':
                return COLOUR_POINT_BURMESE_SPANNED;
            case 'h':
                return COLOUR_POINT_HIMALAYAN_SPANNED;
            case 'f':
                return SCHIMMEL_SPANNED;
            default:
                return String.valueOf(allele);
        }
    }

    static CharSequence getString(CharSequence genotype) {
        StringBuilder builder = new StringBuilder();
        boolean needsHtml = false;
        for (int i = 0; i < genotype.length(); ++i) {
            switch (genotype.charAt(i)) {
                case 'b':
                    builder.append(COLOUR_POINT_BURMESE_HTML);
                    needsHtml = true;
                    break;
                case 'h':
                    builder.append(COLOUR_POINT_HIMALAYAN_HTML);
                    needsHtml = true;
                    break;
                case 'f':
                    builder.append(SCHIMMEL_HTML);
                    needsHtml = true;
                    break;
                default:
                    builder.append(genotype.charAt(i));
                    break;
            }
        }
        return needsHtml ? Html.fromHtml(builder.toString()) : builder.toString();
    }
}
