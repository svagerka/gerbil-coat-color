package info.sandlovers.gerbil.coat.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import info.sandlovers.gerbil.coat.genetics.Color;
import info.sandlovers.gerbil.coat.genetics.ColorDatabase;
import info.sandlovers.gerbil.coat.genetics.Genes;
import info.sandlovers.gerbil.coat.genetics.Genotype;
import info.sandlovers.gerbil.coat.R;

public class GerbilPickerView extends LinearLayout {

    public interface Listener {
        void genotypeChanged();
    }

    private TextView label;
    private ViewGroup genesLayout;
    private Spinner colorSpinner;
    private Listener listener;
    private boolean[] allowedRecessiveGenes;

    private final View.OnClickListener ALLELE_BUTTON_ON_CLICK_LISTENER
            = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // cycle gene value
            Button button = (Button) view;
            char displayed = Genes.getAllele(button.getText());
            char next = Genes.getNextAllele((Integer) view.getTag(), displayed);
            button.setText(Genes.getAlleleLabel(next));

            // update spinner value
            Color color = getCurrentColor();
            if (color != null) {
                ArrayAdapter adapter = (ArrayAdapter) colorSpinner.getAdapter();
                int spinnerPosition = adapter.getPosition(color.name);
                colorSpinner.setSelection(spinnerPosition);
            }

            // notify listener
            if (listener != null) {
                listener.genotypeChanged();
            }
        }
    };

    private final AdapterView.OnItemSelectedListener COLOR_SPINNER_SELECTED_LISTENER =
            new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    String selectedColor = (String) adapterView.getAdapter().getItem(i);
                    Color newColor = ColorDatabase.getColorByName(selectedColor);
                    if (newColor != getCurrentColor()) {
                        setGenotype(newColor.sample);
                        if (listener != null) {
                            listener.genotypeChanged();
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            };

    @SuppressWarnings("UnusedDeclaration")
    public GerbilPickerView(Context context) {
        super(context);
        init(null);
    }

    @SuppressWarnings("UnusedDeclaration")
    public GerbilPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    @SuppressWarnings("UnusedDeclaration")
    @TargetApi(11)
    public GerbilPickerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setAllowedRecessiveGenes(boolean[] allowedRecessiveGenes) {
        this.allowedRecessiveGenes = allowedRecessiveGenes;
        for (int i = 0; i < Genes.CODE_LENGTH; i++){
            getButton(i).setVisibility(allowedRecessiveGenes[i / 2] ? VISIBLE : GONE);
        }

        colorSpinner.setOnItemSelectedListener(null);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                R.layout.spinner_row, ColorDatabase.getAllColorNames(allowedRecessiveGenes));
        colorSpinner.setAdapter(adapter);
        Color color = getCurrentColor();
        if (color != null) {
            colorSpinner.setSelection(adapter.getPosition(color.name));
        }
        colorSpinner.setOnItemSelectedListener(COLOR_SPINNER_SELECTED_LISTENER);
    }

    public Genotype getGenotype() {
        char[] genotype = new char[Genes.CODE_LENGTH];
        for (int i = 0; i < Genes.CODE_LENGTH; i++){
            genotype[i] = allowedRecessiveGenes[i / 2]
                    ? Genes.getAllele(getButton(i).getText()) : Genes.getDominantAllele(i / 2);
        }
        return Genotype.canonized(genotype);
    }

    public void setGenotype(String genotype) {
        for (int g = 0; g < Genes.CODE_LENGTH; g++) {
            getButton(g).setText(Genes.getAlleleLabel(genotype.charAt(g)));
        }
    }

    public Color getCurrentColor() {
        return ColorDatabase.getColorByGenotype(Genotype.dashLess(getGenotype().genotype));
    }

    private void init(AttributeSet attrs) {
        addView(inflate(getContext(), R.layout.gerbil_picker, null));

        label = (TextView) findViewById(R.id.label);

        // setup gene buttons
        genesLayout = (ViewGroup) findViewById(R.id.genes);
        for (int i = 0; i < Genes.CODE_LENGTH; i++){
            Button button = getButton(i);
            button.setText(Genes.getAlleleLabel(Genes.getDominantAllele(i / 2)));
            button.setTag(i / 2);
            button.setOnClickListener(ALLELE_BUTTON_ON_CLICK_LISTENER);
        }

        // setup color spinner
        colorSpinner = (Spinner) findViewById(R.id.color);
        colorSpinner.setAdapter(new ArrayAdapter<String>(getContext(),
                R.layout.spinner_row, ColorDatabase.getAllColorNames(null)));
        colorSpinner.setOnItemSelectedListener(COLOR_SPINNER_SELECTED_LISTENER);

        if (attrs != null) {
            TypedArray a = getContext().getTheme().obtainStyledAttributes(
                    attrs, R.styleable.GerbilPickerView, 0, 0);

            try {
                label.setText(a.getString(R.styleable.GerbilPickerView_label));
            } finally {
                a.recycle();
            }
        }
    }

    private Button getButton(int i) {
        return (Button) genesLayout.getChildAt(i);
    }

}
