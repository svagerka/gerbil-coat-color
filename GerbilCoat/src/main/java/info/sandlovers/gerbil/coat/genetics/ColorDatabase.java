package info.sandlovers.gerbil.coat.genetics;

import android.content.res.Resources;
import android.support.v4.util.LruCache;
import android.util.Log;

import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;

import info.sandlovers.gerbil.coat.R;

public class ColorDatabase {
    private static final Color[] colors  = new Color[] {
            new Color("Agouti","A.C.D.E.G.P."),
            new Color("Nutmeg","aaCCD.e[ef]G.P."),
            new Color("Black","aaC.D.E.G.P."),
            new Color("Dark Eyed Honey (Algerian Fox)","A.CCD.e[ef]G.P."),
            new Color("Argente","A.CCD.E.G.pp"),
            new Color("Red Eyed Honey (Yellow Fox)","A.CCD.e[ef]G.pp"),
            new Color("Lilac","aaCCD.E.G.pp"),
            new Color("Saffron (Red Fox)","aaCCD.e[ef]G.pp"),
            new Color("Dark Tailed White (Himalayan)","..hh......P."),
            new Color("Pink Eyed White","..[hb][hb]......pp"),
            new Color("Dove","aaChD.E.G.pp"),
            new Color("Topaz","A.CbD.E.G.pp"),
            new Color("Light DEH","A.C[hb]D.e[ef]G.P."),
            new Color("Polar Fox","A.CCD.e[ef]ggP."),
            new Color("Light Nutmeg","aaC[hb]D.e[ef]G.P."),
            new Color("Ruby Eyed White","aaC...E.ggpp"),
            new Color("Siamese","aabhD.E.G.P."),
            new Color("LCP Agouti","A.bhD.E.G.P."),
            new Color("Grey Agouti","A.CCD.E.ggP."),
            new Color("Argente Cream","A.ChD.E.G.pp"),
            new Color("Ivory Cream","A.C.D.E.ggpp"),
            new Color("Silver Nutmeg","aaCCD.e[ef]ggP."),
            new Color("CP Agouti","A.bbD.E.G.P."),
            new Color("Slate","aaC.D.E.ggP."),
            new Color("Burmese","aabbD.E.G.P."),
            new Color("LCP Polar Fox (BEW)","..bh..e[ef]ggP."),
            new Color("LCP DEH (BEW)","A.bhD.e[ef]G.P."),
            new Color("LCP Nutmeg","aabhD.e[ef]G.P."),
            new Color("Sapphire","aaCbD.E.G.pp"),
            new Color("CP Nutmeg","aabbD.e[ef]G.P."),
            new Color("CP DEH (BEW)","A.bbD.e[ef]G.P."),
            new Color("Light Saffron","aaC[bh]D.e[ef]G.pp"),
            new Color("Apricot","A.C.D.e[ef]ggpp"),
            new Color("Silver Nutmeg RE","aaC.D.e[ef]ggpp"),
            new Color("Light Grey Agouti","A.C[hb]D.E.ggP."),
            new Color("Light Silver Nutmeg","aaC[hb]D.e[ef]ggP."),
            new Color("Light Polar Fox","A.C[hb]D.e[ef]ggP."),
            new Color("Light REH","A.C[hb]D.e[ef]G.pp"),
            new Color("CP Slate","aabbD.E.ggP."),
            new Color("CP Grey Agouti","A.bbD.E.ggP."),
            new Color("LCP Grey Agouti","A.bhD.E.ggP."),
            new Color("CP Silver Nutmeg","aabbD.e[ef]ggP."),
            new Color("CP Polar Fox (BEW)","A.bbD.e[ef]ggP."),
            new Color("LCP Slate","aabhD.E.ggP."),
            new Color("Dilute Agouti","A.C.ddE.G.P."),
            new Color("Dilute Nutmeg","aaCCdde[ef]G.P."),
            new Color("Blue (Dilute Black)","aaC.ddE.G.P."),
            new Color("Dilute DEH","A.CCdde[ef]G.P."),
            new Color("Dilute Argente","A.CCddE.G.pp"),
            new Color("Dilute REH","A.CCdde[ef]G.pp"),
            new Color("Dilute Lilac","aaCCddE.G.pp"),
            new Color("Dilute Saffron","aaCCdde[ef]G.pp"),
            new Color("Dilute Dove","aaChddE.G.pp"),
            new Color("Dilute Topaz","A.CbddE.G.pp"),
            new Color("Dilute Light DEH","A.C[hb]dde[ef]G.P."),
            new Color("Dilute Polar Fox","A.CCdde[ef]ggP."),
            new Color("Dilute Light Nutmeg","aaC[hb]dde[ef]G.P."),
            new Color("Dilute Siamese","aabhddE.G.P."),
            new Color("Dilute LCP Agouti","A.bhddE.G.P."),
            new Color("Smoke (Dilute Grey Agouti)","A.CCddE.ggP."),
            new Color("Dilute Argente Cream","A.ChddE.G.pp"),
            new Color("Dilute Ivory Cream","A.C.ddE.ggpp"),
            new Color("Dilute Silver Nutmeg","aaCCdde[ef]ggP."),
            new Color("Dilute CP Agouti","A.bbddE.G.P."),
            new Color("Dilute Slate","aaC.ddE.ggP."),
            new Color("Dilute Burmese","aabbddE.G.P."),
            new Color("Dilute LCP DEH (BEW)","A.bhdde[ef]G.P."),
            new Color("Dilute LCP Nutmeg","aabhdde[ef]G.P."),
            new Color("Dilute Sapphire","aaCbddE.G.pp"),
            new Color("Dilute CP Nutmeg","aabbdde[ef]G.P."),
            new Color("Dilute CP DEH (BEW)","A.bbdde[ef]G.P."),
            new Color("Dilute Light Saffron","aaC[bh]dde[ef]G.pp"),
            new Color("Dilute Apricot","A.C.dde[ef]ggpp"),
            new Color("Dilute Silver Nutmeg RE","aaC.dde[ef]ggpp"),
            new Color("Dilute Light Grey Agouti","A.C[hb]ddE.ggP."),
            new Color("Dilute Light Silver Nutmeg","aaC[hb]dde[ef]ggP."),
            new Color("Dilute Light Polar Fox","A.C[hb]dde[ef]ggP."),
            new Color("Dilute Light REH","A.C[hb]dde[ef]G.pp"),
            new Color("Dilute CP Slate","aabbddE.ggP."),
            new Color("Dilute CP Grey Agouti","A.bbddE.ggP."),
            new Color("Dilute LCP Grey Agouti","A.bhddE.ggP."),
            new Color("Dilute CP Silver Nutmeg","aabbdde[ef]ggP."),
            new Color("Dilute CP Polar Fox (BEW)","A.bbdde[ef]ggP."),
            new Color("Dilute LCP Slate","aabhddE.ggP."),
            new Color("Schimmel","..C...ffG.P."),
            new Color("Silver Schimmel (BEW)","..[Cb]...ffggP."),
            new Color("Red Eyed Schimmel","..C...ffG.pp"),
            new Color("CP Schimmel (BEW)","..bb..ffG.P."),
            new Color("LCP Schimmel (BEW)","..bh..ffG.P."),
            new Color("Silver Red Eyed Schimmel (PEW)","..C...ffggpp")
    };
    private static final LruCache<CharSequence, Color> colorCache
            = new LruCache<CharSequence, Color>(1024) {
        @Override
        protected Color create(CharSequence key) {
            for (Color color : colors) {
                Matcher matcher = color.pattern.matcher(key);
                if (matcher.matches()) {
                    return color;
                }
            }
            return null;
        }
    };


    public static Color getColorByGenotype(Genotype genotype) {
        Color color = colorCache.get(CharBuffer.wrap(genotype.genotype));
        if (color == null) {
            Log.e("COLORS", "Genotype " + genotype + " does not match to a color.");
        }
        return color;
    }

    public static List<String> getAllColorNames(boolean[] allowedRecessiveGenes) {
        List<String> colorNames = new ArrayList<String>();
        for (Color color : colors) {
            boolean isColorAllowed = true;
            if (allowedRecessiveGenes != null) {
                for (int i = 0; i < Genes.CODE_LENGTH; ++i) {
                    /* If recessive gene is disallowed, gene can contain only dominant allele
                     * or dash (-) */
                    if (!allowedRecessiveGenes[i/2]
                            && color.sample.charAt(i) != Genes.getDominantAllele(i / 2)
                            && color.sample.charAt(i) != '-') {
                        isColorAllowed = false;
                        break;
                    }
                }
            }

            if (isColorAllowed) {
                colorNames.add(color.name);
            }
        }
        Collections.sort(colorNames);
        return colorNames;
    }

    public static Color getColorByName(String selectedColor) {
        for (Color color : colors) {
            if (color.name.equals(selectedColor)) {
                return color;
            }
        }

        return null;
    }

    public static class ColorChecker {
        private static final int MAX_MATCHING_COLORS = 2;

        private final Color[] matchingColors;
        private final char[] partialCode;
        private final StringBuilder builder;
        private final Resources resources;

        public ColorChecker(Resources resources) {
            this.resources = resources;
            builder = new StringBuilder();
            partialCode = new char[Genes.CODE_LENGTH];
            matchingColors = new Color[MAX_MATCHING_COLORS];

            checkAllRecursive(0);
            Log.e("COLOR_CHECK", builder.toString());
        }

        private void checkAllRecursive(int n) {
            if (Genes.GENE_COUNT == n) {
                check();
                return;
            }

            for (char first = Genes.getDominantAllele(n);
                 first != '-'; first = Genes.getNextAllele(n, first)) {
                partialCode[2*n] = first;
                for (char second = first;
                     second != '-'; second = Genes.getNextAllele(n, second)) {
                    partialCode[2*n+1] = second;
                    checkAllRecursive(n + 1);
                }
            }
        }

        private void check() {
            int matchingColorCount = 0;
            CharBuffer code = CharBuffer.wrap(partialCode);
            for (Color color : colors) {
                Matcher matcher = color.pattern.matcher(code);
                if (matcher.matches()) {
                    if (matchingColorCount < MAX_MATCHING_COLORS) {
                        matchingColors[matchingColorCount] = color;
                    }
                    ++matchingColorCount;
                }
            }

            switch (matchingColorCount) {
                case 0:
                    builder.append(resources.getString(R.string.check_colors_0_matches, code));
                    break;
                case 1:
                    // 1 match is perfect
                    break;
                case 2:
                    builder.append(resources.getString(R.string.check_colors_2_matches, code,
                            matchingColors[0].name, matchingColors[1].name));
                    break;
                default:
                    builder.append(resources.getString(R.string.check_colors_n_matches, code,
                            matchingColors[0].name, matchingColors[1].name,
                            matchingColorCount - MAX_MATCHING_COLORS));
                    break;
            }
        }
    }


}
