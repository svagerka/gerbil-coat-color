package info.sandlovers.gerbil.coat.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.ShareActionProvider;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import info.sandlovers.gerbil.coat.genetics.Children;
import info.sandlovers.gerbil.coat.genetics.Color;
import info.sandlovers.gerbil.coat.genetics.ColorDatabase;
import info.sandlovers.gerbil.coat.genetics.Genes;
import info.sandlovers.gerbil.coat.genetics.Genotype;
import info.sandlovers.gerbil.coat.view.GerbilPickerView;
import info.sandlovers.gerbil.coat.R;

public class MainActivity extends ActionBarActivity implements GerbilPickerView.Listener {

    private static final String[] CHILDREN_ADAPTER_FROM = new String[]
            {Children.KEY_COLOR_NAME, Children.KEY_COLOR_PERCENTAGE, Children.KEY_GENES};
    private static final int[] CHILDREN_ADAPTER_TO = new int[]
            {R.id.child_color, R.id.child_percentage, R.id.child_genes};
    private static final SimpleAdapter.ViewBinder SPANNING_VIEW_BINDER =
            new SimpleAdapter.ViewBinder() {
                @Override
                public boolean setViewValue(View view, Object data, String textRepresentation) {
                    if (data instanceof Spanned && view instanceof TextView) {
                        ((TextView) view).setText((Spanned) data);
                        return true;
                    } else {
                        return false;
                    }
                }
    };

    private View loadingPanel;
    private View dashWarningLabel;
    private ShareActionProvider shareActionProvider;
    private GerbilPickerView mom;
    private GerbilPickerView dad;
    private ListView childrenView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadingPanel = findViewById(R.id.loadingPanel);
        dashWarningLabel = findViewById(R.id.dash_warning_label);
        childrenView = (ListView) findViewById(R.id.children_view);
        mom = (GerbilPickerView) findViewById(R.id.mother);
        dad = (GerbilPickerView) findViewById(R.id.father);

        mom.setListener(this);
        dad.setListener(this);

        /* Uncomment for colour database check. */
        // new ColorDatabase.ColorChecker(getResources());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem shareItem = menu.findItem(R.id.menu_item_share);
        shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.help:
                startActivity(new Intent(this, HelpActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        boolean[] allowedGenes = new boolean[Genes.GENE_COUNT];
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        allowedGenes[0] = prefs.getBoolean(getString(R.string.pref_recessive_a), true);
        allowedGenes[1] = prefs.getBoolean(getString(R.string.pref_recessive_c), true);
        allowedGenes[2] = prefs.getBoolean(getString(R.string.pref_recessive_d), false);
        allowedGenes[3] = prefs.getBoolean(getString(R.string.pref_recessive_e), true);
        allowedGenes[4] = prefs.getBoolean(getString(R.string.pref_recessive_g), false);
        allowedGenes[5] = prefs.getBoolean(getString(R.string.pref_recessive_p), true);
        mom.setAllowedRecessiveGenes(allowedGenes);
        dad.setAllowedRecessiveGenes(allowedGenes);

        genotypeChanged();
    }

    @Override
    public void genotypeChanged() {
        loadingPanel.setVisibility(View.VISIBLE);
        new RecalculateChildrenTask().execute(mom.getGenotype(), dad.getGenotype());
    }

    private class RecalculateChildrenTask extends AsyncTask<Genotype, Integer, Children> {
        private boolean containsDash;
        private Genotype father;
        private Genotype mother;

        @Override
        protected Children doInBackground(Genotype... genotypes) {
            mother = genotypes[0];
            father = genotypes[1];
            containsDash =
                    -2 != (new String(mother.genotype).indexOf('-')
                            + new String(father.genotype).indexOf('-'));
            return new Children(mother, father);
        }

        @Override
        protected void onPostExecute(final Children data) {
            SimpleAdapter adapter = new SimpleAdapter(MainActivity.this, data,
                    R.layout.child_list_item, CHILDREN_ADAPTER_FROM, CHILDREN_ADAPTER_TO);
            adapter.setViewBinder(SPANNING_VIEW_BINDER);
            childrenView.setAdapter(adapter);

            if (shareActionProvider != null) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND)
                        .setAction(Intent.ACTION_SEND)
                        .putExtra(Intent.EXTRA_TEXT, getStringExtra(data))
                        .setType("text/plain");
                shareActionProvider.setShareIntent(shareIntent);
            }
            loadingPanel.setVisibility(View.GONE);
            dashWarningLabel.setVisibility(containsDash ? View.VISIBLE : View.GONE);
        }

        private String getStringExtra(Children data) {
            Color motherColor = ColorDatabase.getColorByGenotype(mother);
            String motherColorName = motherColor != null
                    ? motherColor.name : getString(R.string.unknown_color);
            Color fatherColor = ColorDatabase.getColorByGenotype(father);
            String fatherColorName = fatherColor != null
                    ? fatherColor.name : getString(R.string.unknown_color);
            return new StringBuilder()
                    .append(getString(R.string.mother_label)).append(" ")
                    .append(mother.toString()).append(" (")
                    .append(motherColorName).append(")\n")
                    .append(getString(R.string.father_label)).append(" ")
                    .append(father.toString()).append(" (")
                    .append(fatherColorName).append(")\n\n")
                    .append(getString(R.string.children_label)).append('\n')
                    .append(data.toString())
                    .toString();
        }

    }

}
